import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

/*
export default function Banner(){
	return (
		<Row>
			<Col>
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere!</p>
				<Button variant="primary">Enroll now!</Button>
			</Col>
		</Row>
	)
}*/

export default function Banner({bannerProp}){

	console.log(bannerProp)
	const {title, content, destination, label} = bannerProp;

	return(
		// text-center to place all text and component in the center.
		// my = mt and mb = margin top and margin bottom
		<div className="text-center my-5">
			<Row>
				<Col className="p-5 text-center">
					<h1>{title}</h1>
					<p>{content}</p>
					<Button as={Link} to={destination} variant="primary">{label}</Button>
				</Col>
			</Row>
		</div>
	)
}