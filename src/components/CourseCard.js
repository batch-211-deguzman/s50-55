// import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';


export default function CourseCard({courseProp}){
	// console.log(props);

	//const [count, setCount] = useState(0);

	let {_id, name, description, price} = courseProp;

	//const [seats, setSeats] = useState(10);

	// function enroll(){
		// if(seats > 0 ){
		// 	setCount(count + 1)
		// 	console.log("Enrollees: " + count);

		// 	setSeats(seats - 1)
		// 	console.log("Seats: " + seats);
		// }
		// else{
		// 	alert("Maximum Enrollees reached.");
		// }

		//refactor 
		// if(count!==10){
		//	setCount(count +1);
		//	setSeats(seats - 1);
	//	}
//	}

	// useEffect(()=>{
	// 	if(seats===0){
	// 		alert("No more seats Available!")
	// 	}
	// },[count,seats])


	return(
		<Card className="p-3 mb-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>
				<Button as={Link} to={`/courses/${_id}`}>Details</Button>
			</Card.Body>
		</Card>


	)
}
